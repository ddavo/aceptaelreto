#!/bin/bash
sum=$(ls -l 402_*.dat | wc -l)
paste -d"\t" 402_*.dat | nawk -v s="$sum" '{
    for ( i = 0; i<=s-1;i++) {
        if ($(3+i*7) > 0) {
            c++
        }
        
        t1 = 3+(i*7)
    	sumAC = sumAC + $t1
    	t2 = 5+(i*7)
    	sumTLE = sumTLE + $t2
    	t3 = 7+(i*7)
    	sumsqrt = sumsqrt + $t3
    }

    print $1"\t"$2"\t"sumAC/c"\t"$4"\t"sumTLE/c"\t"$6"\t"sumsqrt/c"\t"c
    sumAC = 0
    sumTLE = 0
    sumsqrt = 0
    c = 0
}'
