#include <iostream>

using namespace std;

typedef unsigned long long uloong;

// Pre: { 0<=i<N ^ (forall i : 0<=i<N : 0<V[i]<2^32) }
int solve(uloong V[], int N) {
    int i, cImpares = 0;
    bool pares = true;
    for (i = 0; i < N && pares; i++) {
        if (V[i]%2) {
            pares = false;
            cImpares = i;
        }
    }

    for (i; i < N && !pares; i++) {
        if (V[i]%2 == 0) pares = true;
    }

    return pares?-1:(N - cImpares);
}
// Pos: { forall i : 0<=i<k<N : par(i) } ^ r = {#j : k<=j<N : impar(j)}

int main() {
    int n, m;
    cin >> n;
    for (int i = 0; i < n; i++) {
        cin >> m;

        uloong * arr = new uloong [m];
        for (int j = 0; j < m; j++) cin >> arr[j];
        if ((m = solve(arr, m)) >= 0){
            cout << "SI " << m << "\n";
        } else {
            cout << "NO\n";
        }

        delete [] arr;
    }

    return 0;
}