#include <iostream>
using namespace std;

int solve(int n) {
    if (n>1) {
        return (n+solve(n/2))*4;
    } else {
        return 4;
    }
}

int main() {
    int n;
    while (cin >> n) {
        cout << solve(n) << "\n";
    }


    return 0;
}