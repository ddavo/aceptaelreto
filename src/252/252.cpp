#include <iostream>
#include <string>

#include <ctype.h>
#include <cstdlib>
#include <stdio.h>
bool solve(char * start, char * end) {
    if (start < end) {
        if (*start == ' ') return solve(start + 1, end);
        else if (*end == ' ') return solve(start, end -1);
        else return tolower(*start) == tolower(*end) && solve(start+1, end-1);
    } else return tolower(*start)==tolower(*end);
}

bool solve(std::string str, int start, int end) {
    if (start < end) {
        if (str[start] == ' ') return solve(str, start + 1, end);
        else if (str[end] == ' ') return solve(str, start, end -1);
        else return tolower(str[start]) == tolower(str[end]) && solve(str, start+1, end-1);
    } else return tolower(str[start])==tolower(str[end]);
}


int main() {
    std::string str;
    getline(std::cin, str);
    while(str != "XXX") {
        std::cout << (solve(str, 0, str.length()-1)?"SI\n":"NO\n");
        getline(std::cin, str);
    }

    /*
    char * str = (char*)malloc(110*sizeof(char));
    size_t maxsize = 110;
    ssize_t strsize;
    strsize = getline(&str, &maxsize, stdin);
    while (str[0] != 'X' && str[1] != 'X' && str[2] != 'X') {
        printf(solve(str, str+strsize-2)?"SI\n":"NO\n");

        strsize = getline(&str, &maxsize, stdin);
    }*/

    return 0;
}