#include <cstdio>
#include <cstring>

const int k = 46337;

void mul(int M1[2][2], int M2[2][2]) {
    long long a =  (M1[0][0]*M2[0][0]%k + M1[0][1]*M2[1][0]%k); 
    long long b =  (M1[0][0]*M2[0][1]%k + M1[0][1]*M2[1][1]%k); 
    long long c =  (M1[1][0]*M2[0][0]%k + M1[1][1]*M2[1][0]%k); 
    long long d =  (M1[1][0]*M2[0][1]%k + M1[1][1]*M2[1][1]%k); 

    M1[0][0] = a%k;
    M1[0][1] = b%k;
    M1[1][0] = c%k;
    M1[1][1] = d%k;
}

void pown(int M[2][2], int n) {
    int aux [2][2];
    memcpy(aux, M, sizeof(int)*4);

    for (int i = 2; i < n; i++) {
        mul(M, aux);
    }
}

void powlog(int M[2][2], int n) {
    if (n == 1 || n == 0) return;

    int aux [2][2];
    memcpy(aux, M, sizeof(int)*4);

    powlog(M, n/2);
    mul(M,M);

    if (n%2) 
        mul(M,aux);
}

int fib(int n) {
    int M[2][2] = { 
        {0,1},
        {1,1}
    };
    if (n == 0)
        return 0;

    powlog(M, n-1);
    return M[1][1];
}

int main() {
    int f;
    scanf("%d\n", &f);
    while(f) {
        printf("%d\n", fib(f));
        scanf("%d\n", &f);
    }

    return 0;
}