#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main() {
	int n, m;
	cin >> n;
	for (int i = 0; i < n; i++) {
		cin >> m;
		switch(m) {
		        case 0:
		        case 1: cout << 1; break;
         		case 2: cout << 2; break;
		        case 3: cout << 6; break;
		        case 4: cout << 4; break;
			default: cout << 0;
		}
		cout << endl;
	}
	
	return 0;
}
