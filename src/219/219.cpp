#include <iostream>

using namespace std;

// RESUELTO EN CLASE 2018/09/25

/*
La Complejidad es de O(n)
*/
// { Pre: 0<=n<=long(v) }
int solve(int v[], int n) {
    int r = 0, i = 0;
    while (i < n) {
        if (v[i]%2==0) r++;
        i++
    }

    return r;
}
// { Post: r = # i : 0<=i<n : v[i] mod 2 = 0 }

void resuelve() {
    int m;
    cin >> m;
    int * v = new int [m];
    for (int j = 0; j < m; j++) cin >> v[j];
    cout << solve(v, m) << "\n";

    delete [] v;
}

int main() {
    int n;
    cin >> n;
    for (int i = 0; i < n; i++) {
        resuelve();
    }

    return 0;
}