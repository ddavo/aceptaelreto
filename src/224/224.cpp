#include <iostream>

using namespace std;

// Pre: {0<n<=2^32^{forall i : 0<=i<n : 0<V[i]<2^64 }}
unsigned long long solve(unsigned long long V[], unsigned n) {
    unsigned i = 0, sum = 0, index = 0;
    for (i = n; i > 0; i--) {
        if (V[i-1] == sum) index = i;
        sum += V[i-1];
    }

    return index;
}
// Pos: r = {existe i : 0<=i<n : V[i] = {sum j : i<=j<n : V[j] }}

int main() {
    unsigned n;
    cin >> n;
    while (n) {
        unsigned long long * arr = new unsigned long long [n];
        for (int i = 0; i < n; i++) {
            cin >> arr[i];
        }

        if ( n = solve(arr, n) ) {
            cout << "SI " << n << "\n";
        } else {
            cout << "NO\n";
        }

        delete [] arr;
        cin >> n;
    }

    return 0;
}