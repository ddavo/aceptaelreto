#include <cstdio>
#define BASE 6

void solve(int n) {
    if (n < BASE) {
        putchar(n+'0');
    } else {
        solve(n/BASE);
        putchar(n%BASE+'0');
    }
}

int main() {
    int n, m;
    scanf("%d\n", &n);
    for (int i = 0; i < n; i++) {
        scanf("%d\n", &m);
        solve(m);
        putchar('\n');
    }

    return 0;
}