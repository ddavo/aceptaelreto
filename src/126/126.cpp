#include <iostream>

using namespace std;

bool solve(int p, int n) {
    if (n == 0) n = 1;
    return p<=n;
}

int main() {
    int p, n;
    cin >> p >> n;
    while (p>=0 && n>=0) {
        cout << (solve(p, n)?"YES":"NO") << "\n";

        cin >> p >> n;
    }

    return 0;
}