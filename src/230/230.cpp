#include <cstdio>
#include <cstring>
#include <iostream>

const int MAX_SIZE = 100000;

long long mezcla(int v[], int a, int m, int b) {
    // int * aux = new int [b-a+1];
    long long cnt = 0;

    int i = 0, j = m-a+1, k = a;
    // memcpy(aux, &v[a], (b-a+1)*sizeof(int));

    while ( i <= m-a && j <= b-a ) {
        if (v[i] <= v[j]) {
            // v[k] = aux[i];
            i++;
        } else {
            // v[k] = aux[j];
            j++;
            cnt+=m-i;
        }
        k++;
    }

    /*
    while (i+a <= m) {
        v[k] = aux[i];
        i++;
        k++;
    }

    while (j+a <= b) {
        v[k] = aux[j];
        j++;
        k++;
    }
    */

    // printf("i: %d, j: %d, k: %d, cnt: %d\n", i, j, k, cnt);
    return cnt;
}

long long solve(int v[], int a, int b) {
    long long i = 0;
    if (a < b) {
        int m = (a+b)/2;
        // La creación de los dos nuevos arrays necesarios para el merge
        // la hacemos en la función mezcla()

        i = solve(v, a, m);
        i += solve(v, m+1, b);

        // printf("a: %d, m: %d, b %d, i: %d\n", a, m, b, i);
        i += mezcla(v, a, m, b);
    }

    return i;
}

long long solve(int v[], int n) {
    long long r = solve(v, 0, n-1);
    return r?r+1:r;
}

int main() {
    int n;
    int v[MAX_SIZE];
    scanf("%d\n", &n);
    while (n) {
        for (int i = 0; i < n; i++)
            scanf("%d", &v[i]);

        // printf("%d\n", solve(v, n));
        std::cout << solve(v,n) << "\n";
        scanf("%d\n", &n);
    }

    return 0;
}