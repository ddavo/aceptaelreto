#include <cstdio>

int maxindex(const int v[], int a, int b) {
    int imax = a;
    for (int i = a; i <= b; i++)
        if (v[i] > v[imax]) imax = i;

    return imax;
}

int minindex(const int v[], int a, int b) {
    int imin = a;
    for (int i = a; i <= b; i++)
        if (v[i] < v[imin]) imin = i;

    return imin;
}

bool solve(const int v[], int a, int b) {
    if (b - a <= 1) return true;  // Caso base

    int p = maxindex(v, a, b);

    if (p >= b) // El pivote está a la dcha, nos movemos hacia la izda
        return solve(v, a, b-1);

    int min = minindex(v, a, p);    // Cogemos el minimo de los de la izquierda
    int max = maxindex(v, p+1, b);  // Cogemos el máximo de los de la dcha

    if (v[min] < v[max] && v[max] < v[p]) return false; // Hemos encontrado un caso

    // Comprobamos que tanto a la dcha como a la izda sea "true"
    return solve(v, a, p) && solve(v, p+1, b);
}

bool solve(const int v[], int n) {
    return solve(v, 0, n-1);
}
// Retorna True si no tienen que elegir otra combinación (Siempre premio)

int main() {
    int n;
    int v[500000];
    while(scanf("%d", &n) != EOF) {
        for (int i = 0; i < n; i++) 
            scanf("%d", &v[i]);

        printf(solve(v, n)?"SIEMPRE PREMIO\n":"ELEGIR OTRA\n");
    }

    return 0;
}