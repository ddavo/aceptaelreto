#include <iostream>

bool solve(long long int n, int * aux) {
    // CASO BASE:
    if (n < 10) {
        *aux = 1;
        return true;
    } else {
        bool tmp = solve(n/10, aux);
        (*aux)++;
        return tmp && n%*aux == 0;//  && n%aux == 0;
    }
}

bool solve(long long int n) {
    // GENERALIZACIÓN
    int aux;
    return solve(n, &aux);
}

int main() {
    long long int n;
    while (std::cin >> n) {
        std::cout << (solve(n)?"POLIDIVISIBLE\n":"NO POLIDIVISIBLE\n");
    }

    return 0;
}