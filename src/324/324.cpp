#include <cstdio>
#include <algorithm>

const int MAX_SIZE = 100;
int v[MAX_SIZE];
bool b[MAX_SIZE];
int m;

int mcm(const int a, const int b) {
    int aux = b, a1 = a, b1 = b;
    while (b1) {
        aux = b1;
        b1 = a1%aux;
        a1 = aux;
    }

    return a*b/a1;
}

bool nexti(bool b[], int n, int & i) {
    i = 0;
    while (i < n && b[i]) ++i;
    return i < n;
}

int solve(const int n) {
    int i;
    int ret = 1;

    while (nexti(b, n, i)) {
        
        int cnt = 1;
        int j = i+1;
        b[i] = true;

        while (v[i] != j) {
            i = v[i]-1;
            b[i] = true;
            cnt++;
        }

        ret = mcm(ret, cnt);
    }

    return ret;
}

int main() {
    scanf("%d\n", &m);

    while (m) {
        int i = 0;
        while(i<m) {
            scanf("%d", &v[i++]);
            b[i] = 0;
        }
        printf("%d\n", solve(m));
        scanf("%d", &m);
    }

    return 0;
}