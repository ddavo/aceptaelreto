#include <stdio.h>
#include <math.h>
#include <time.h>

int solveAC(int n) {
    int a = std::sqrt(n);

    while (n % a > 0)
        a--;
    
    return n/a;
}

int solveTLE(int n) {
    int x, i, min;
    x = i = min = std::sqrt(n);
    while ( x*min != n ) {
        i++;
        if (x-i < min)
            min = i;
        
        x = n/i;
    }

    return min;
}

int main(int argc, char * argv[]){
    FILE * f = fopen(argv[1], "a+");
    printf("Printing to file %s", argv[1]);
    int r1, r2, r3;
    timespec start, end;
    unsigned long long int t1, t2, t3;

    for (int i = 1; i < 500000000; i+=1000) {
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start);
        r1 = solveAC(i);
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end);
        t1 = end.tv_nsec - start.tv_nsec;

        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start);
        r2 = solveTLE(i);
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end);
        t2 = end.tv_nsec - start.tv_nsec;

        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start);
        r3 = sqrt(i);
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end);
        t3 = end.tv_nsec - start.tv_nsec;

        fprintf(stdout, "%d\t%d\t%ju\t%d\t%ju\t%d\t%ju\n", i, r1, t1, r2, t2, r3, t3);
        fprintf(f, "%d\t%d\t%ju\t%d\t%ju\t%d\t%ju\n", i, r1, t1, r2, t2, r3, t3);
        if (r1 != r2) return 1;
    }
    
    return 0;
}