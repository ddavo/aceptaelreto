#include <iostream>
#include <string>
using namespace std;

const int MAX_SIZE = 250000;

// { Pre: (1 <= n <= longitud(v)) ^ (sum > 0) ^ (P.t. i : 0 <= i < n : v[i] > 0) } 
long long int sumaClave (char v[], long long unsigned n, long long int sum) {
    long long int ret = 0;
    long long int i = 0, primero = 0, sump = 0;
    while (i < n) {
        sump += v[i] - '0';
        
        while (sump > sum) {
            sump -= v[primero] - '0';
            ++primero;
        } 
        if (sump == sum) {
            sump -= v[primero] - '0';
            ++primero;
            ++ret;
        }

        ++i;
    }

    return ret;
}
// {Post: ret = # i, j : 0 <= i <= j < n : (sum k : i <= k <= j : v[k]) = sum}

int main() {
    long long unsigned n, m, j;
    char strNumeros[MAX_SIZE];
    // strNumeros.reserve(MAX_SIZE);
    cin >> n;
    for (long long unsigned i = 0; i < n; i++) {
        cin >> m;

        getchar();
        strNumeros[0] = getchar();
        j = 1;
        while (j < MAX_SIZE && strNumeros[j-1] != '\n') {
            strNumeros[j++] = getchar();
        }
        // getline(cin, strNumeros);
        // cin >> strNumeros;
        cout << sumaClave(strNumeros, j, m) << "\n";
    }


    return 0;
}