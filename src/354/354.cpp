#include <stdio.h>
using namespace std;

const int MAX_CASOS = 200000;

// { Pre: 1<=n<=tamaño(v) ^ p.t k : 0<=k<n : v[k] > 0 }
int solve(unsigned v[], int n) {
    int ret = 1;    // Hay al menos un niño
    int max, max2;
    max = max2 = v[0];

    int j = 1;
    while (j < n) {
        if (v[j] > max2) max2 = v[j];
        if (v[j] <= max) {
            max = max2;
            ret = j+1;
        }

        ++j;
    }

    return ret;
}
// { Ret : min j: 0<=j<n ^ (v[j] < ) : j+1 }

int main() {
    int cdp, i;
    unsigned v[MAX_CASOS];
    scanf("%d", &cdp);
    while (cdp != 0) {
        i = 0;
        while (i < cdp)
        {
            scanf("%d", v + i++);
        }

        printf("%d\n", solve(v, cdp));

        scanf("%d", &cdp);
    }

    return 0;
}