#include <iostream>
#include <string>
using namespace std;

long long solve(long long int n) {
    long long S = 0;
    if(n) {
        S = solve(n/10) + n%10;
        if (n >= 10) cout << " + ";
        cout << n % 10;
    }

    return S;
}

int main() {
    long long int n;
    cin >> n;
    while (n >= 0) {
        if (n==0) cout << "0";
        n = solve(n);
        cout << " = " << n << "\n";
        cin >> n;
    }

    return 0;
}