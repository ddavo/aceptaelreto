#include <iostream>
using std::cin;
using std::cout;
using std::endl;

int queHago(int n){
    return 1<<n;
    // return 
}

// Lo de total = sum... es para disminuir el Tiempo de 1Ejecución
// { 0<=nPaises<=tamaño(arr) ^ total = sum i : 0<=i<nPaises : arr[i] }
long long int solve(const int arr[], long long int total, const int nPaises) { /*return S1 */
    long long int S1 = 0;

    // { I : 0<=i<nPaises ^
    // total = (sum j : 0<=j<nPaises-i : arr[j]) ^
    // S1 = sum k,j : 0<=k<j<i : v[k]*v[j]
    // }
    int i = 0;
    while (i < nPaises-1) {
        total -= arr[i];
        S1 += arr[i] * total;
        i++;
    }

    return S1;
}
// { Post: S1 = sum i,j : 0<=i<j<nPaises : v[i]*v[j] }

int main() {
    int nPaises;

    cin >> nPaises;
    while (nPaises) {
        int * arr = new int [nPaises];
        long long int total = 0;
        for (int i = 0; i < nPaises; i++) {
            cin >> arr[i];
        	total += arr[i];
        }

        cout << solve(arr, total, nPaises) << endl;

        delete [] arr;
        cin >> nPaises;
    }

    return 0;
}
