#include <iostream>

using namespace std;

int main() {
	int n, k;
	cin >> n >> k;
	while (n) {
		cout << ((n-1)/k+1)*10 << "\n";

		cin >> n >> k;
	}

	return 0;
}