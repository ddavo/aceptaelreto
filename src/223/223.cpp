#include <iostream>
using namespace std;
const int MAX_SIZE = 10000;

void solve(int v[], int n, int * a, int * b) {
    int vi = 0, vf = 0;
    int maxw = 0;
    *a = n;
    *b = 0;
    while (vf < n) {
        if (v[vf] == 0) {
            vi++;
        }
        if (vf - vi > maxw) {
            maxw = vf-vi;
            *a = vi; *b = vf;
        }

        vf++;
    }
}

int main() {
    int v[MAX_SIZE];
    int cdp, n, a, b;
    cin >> cdp;
    for (int i = 0; i < cdp; i++) {
        cin >> n;
        for (int j = 0; j < n; j++) cin >> v[j];

        solve(v, n, &a, &b);
        if (a <= b) {
            printf("%d -> [%d,%d]\n", b-a+1, a,b);
        } else {
            printf("SIGUE BUSCANDO\n");
        }
    }    

    return 0;
}
