#include <iostream>

using namespace std;

// long long int megaArr[50000];

// { Pre: 0<=n<=tamaño(v) }
/*bool solve(long long int v [], int n) {
    // { I : 1<=i<=n
    //       i = # k : 1<=k<i : v[k] > v[k-1]
    // }
    int i = 1;
    while (i < n && v[i] > v[i-1]) {
        i++;
    }

    return i == n;
}
*/
// { Post : P.t k : 1<=k<n : v[k] > v[k-1]
// ESTOY HASTA LOS HUEVOS DEL RTE, PROCESARE LOS DATOS CONFORME RECIBO LA ENTRADA

int main() {
    long long int n, anterior, actual;
    bool f1, f2;
    cin >> n;
    while (n > 0){
        f1 = f2 = true;
        cin >> anterior;
        for (long long int i = 1; i < n; i++){
            cin >> actual;
            f1 = f1 && anterior < actual;
            f2 = f2 && anterior > actual;

            anterior = actual;
        }

        cout << ((f1||f2)?"DALTON\n":"DESCONOCIDOS\n");
        cin >> n;
    }

    return 0;
}