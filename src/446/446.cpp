#include <iostream>
#include <string>
using namespace std;

// { Pre: 0 <= n <= long(nombresDichos) }
bool esAbuelaVerdadera(const std::string &nombreReal, const std::string nombresDichos[], int n) /* return ret; */ {
    bool ret = (n > 1 && nombreReal == nombresDichos[n-1]);

    int i = 0;
    while (i < n-1 && (ret = ret && nombreReal != nombresDichos[i])) i++;

    return ret;
}
// { Pos: ret = (n > 1) ^ ( nombresDichos[n-1] = nombreReal ) 
// ^ (p.t. k : 0<=k<n-1 : nombreReal != nombresDichos[k])

int main() {
    int n, m;
    string nombreVerdadero;
    string nombresDichos[100];
    cin >> n;
    for (int i = 0; i < n; i++) {
        cin >> nombreVerdadero >> m;
        for (int j = 0; j < m; j++)
            cin >> nombresDichos[j];
        cout << (esAbuelaVerdadera(nombreVerdadero, nombresDichos, m)?"VERDADERA\n":"FALSA\n");
    }
}