#include <iostream>

using namespace std;

int max(int n1, int n2) {
    return (n1 > n2) ? n1 : n2;
}

int main() {
    int n, aux, maximo;
    while (cin >> n) {
        maximo = 0;
        for (int i = 0; i < n; i++) {
            cin >> aux;
            maximo = max(maximo, aux);
        }
        cout << maximo << "\n";
    }

    return 0;
}