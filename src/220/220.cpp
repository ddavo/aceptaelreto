#include <cstdio>

// Resuelto en Python
const bool res[31][9] = {
    { 1, 1, 1, 0, 1, 1, 1, 1, 1 }, // 1 
    { 1, 1, 1, 1, 1, 1, 1, 1, 0 }, // 2 
    { 0, 0, 0, 0, 0, 0, 0, 0, 0 }, // 3 
    { 1, 0, 1, 1, 0, 1, 1, 1, 1 }, // 4 
    { 1, 1, 1, 0, 1, 1, 1, 1, 1 }, // 5 
    { 1, 1, 1, 0, 1, 1, 1, 1, 1 }, // 6 
    { 1, 1, 1, 1, 1, 1, 1, 1, 1 }, // 7 
    { 0, 0, 1, 1, 1, 1, 1, 1, 1 }, // 8 
    { 1, 0, 0, 0, 1, 1, 1, 0, 0 }, // 9 
    { 1, 1, 1, 0, 1, 1, 1, 1, 1 }, // 10 
    { 1, 1, 1, 1, 1, 1, 0, 1, 0 }, // 11 
    { 1, 0, 1, 1, 1, 1, 1, 1, 1 }, // 12 
    { 1, 0, 0, 0, 1, 1, 1, 0, 0 }, // 13 
    { 1, 0, 1, 0, 1, 0, 0, 1, 0 }, // 14 
    { 1, 1, 1, 1, 1, 1, 1, 1, 0 }, // 15 
    { 0, 0, 1, 0, 0, 1, 1, 1, 0 }, // 16 
    { 1, 0, 1, 0, 1, 1, 1, 1, 0 }, // 17 
    { 1, 1, 1, 1, 1, 0, 1, 1, 1 }, // 18 
    { 0, 1, 1, 0, 1, 1, 1, 1, 1 }, // 19 
    { 1, 1, 1, 1, 0, 1, 1, 1, 1 }, // 20 
    { 0, 1, 1, 0, 1, 1, 1, 1, 1 }, // 21 
    { 1, 1, 1, 1, 1, 1, 1, 1, 1 }, // 22 
    { 1, 1, 1, 1, 1, 1, 1, 1, 1 }, // 23 
    { 1, 1, 1, 1, 1, 1, 1, 1, 1 }, // 24 
    { 0, 1, 0, 1, 0, 1, 0, 1, 0 }, // 25 
    { 1, 0, 0, 0, 1, 1, 1, 0, 0 }, // 26 
    { 1, 1, 0, 0, 0, 1, 0, 0, 1 }, // 27 
    { 1, 1, 1, 1, 1, 0, 1, 1, 0 }, // 28 
    { 0, 1, 1, 1, 0, 0, 1, 0, 0 }, // 29 
    { 0, 0, 0, 0, 0, 0, 0, 0, 0 }, // 30 
    { 1, 1, 1, 1, 1, 1, 1, 1, 1 } // 31
};

bool solve(int suma, int anterior) {
    return res[suma-1][anterior-1];
}

inline void readInt(int *n) {
    *n = 0;
    for (register int c = getchar_unlocked(); c>='0' && c<='9'; c = getchar_unlocked())
        *n = *n*10+c-'0';
}

int main() {
    int n, m, k;
    readInt(&n);
    for (int i = 0; i < n; i++) {
        readInt(&m);
        readInt(&k);
        printf(solve(m, k)?"GANA\n":"PIERDE\n");
    }

    return 0;
}