#include <iostream>
#include <algorithm>

using namespace std;

// Pre: {1 <= size <= tamaño(mountains) <= 100.000 ^ (forall i : 0 <= i < size : 0 < mountains[i] < 2^32)}
unsigned solve(unsigned mountains[], const unsigned size){
	unsigned result = 0, mx = 0; // Asumimos que el ultimo es maximo tambien

	for(unsigned i = 1; i <= size; i++){
		if(mountains[size-i] > mx){
			mx = mountains[size-i];
			result++;
		}
	}

	return result;
}
// Pos: {#i : 0<=i<N : mountains[i] == {max j : i<=j<N: mountains[j]} }

int main(){
	unsigned size;
	cin >> size;
    while(size){
    	unsigned mountains[size];
    	for(unsigned i = 0; i < size; i++){
    		cin >> mountains[i];
    	}
    	cout << solve(mountains, size) << "\n";
    	cin >> size;
    }

    return 0;
}