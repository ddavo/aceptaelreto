#include <cstdio>
using namespace std;

const int MAX_PASAJEROS = 1000000;
int v[MAX_PASAJEROS];

/*
// { Pre: 1 <= posicion < size-1 <= size(v) }
int consultar(int v[], int size, int posicion) {
    return v[posicion - 1];
}
// { Pos: ret = v[posicion-1] }
    
// {Pre: 0<=pasajeros<size }
int embarcar(int v[] in v, out V , int size, int cubierta, int pasajeros) {

    // C = pasajeros - i
    // { I : 0<=i<=pasajeros ^
    //       j = cont(v,i,cubierta) ^
    //       p.t k : 0<=k<i : V[k-j] = v[k];
    // }
    register int i = 0, j = 0;
    while (i < pasajeros) {
        if (v[i] == cubierta) {
            ++j;
            
        } else {
            v[i-j] = v[i];
        }
        i++;
    }

    return pasajeros-j;
}
// cont(v, n, l) = # k : 0<=k<n : v[k] /= l
// { Pos: ret = cont(v, pasajeros, cubierta) ^
//       ^ p.t k : 0<=k<pasajeros : V[k-cont(v,k,cubierta)] = v[k]
// }
*/


void readInt(int *n) {
    *n = 0;
    for (register int c = getchar_unlocked(); c>='0' && c<='9'; c = getchar_unlocked())
        *n = *n*10+c-'0';
}

void readAccion(char *c) {
    *c = getchar_unlocked();
    for (register int i = 0; i < 8; i++)
        getchar_unlocked();
}

void printInt(int n) {
    if (n/10) printInt(n/10);
    putchar_unlocked(n%10+'0');
}

int main() {
    int pasajeros, acciones, destino, pasajerosIni;
    char accion;
    readInt(&pasajerosIni);

    while (pasajeros = pasajerosIni) {
        // CASO DE PRUEBA
        for (int i = 0; i < pasajerosIni; i++)
            readInt(&v[i]);

        readInt(&acciones);
        for (int i = 0; i < acciones; i++) {
            readAccion(&accion);
            readInt(&destino);
            if (accion == 'C') printInt(v[destino-1]);
            else {
                int k = 0, j = 0;
                while (k < pasajeros) {
                    if (v[k] == destino) {
                        ++j;
                    } else {
                        v[k-j] = v[k];
                    }
                    k++;
                }

                printInt(pasajeros-=j);
            } 
            // printInt(pasajeros = embarcar(v, pasajerosIni, destino, pasajeros));
            putchar_unlocked('\n');
        }

        putchar_unlocked('*');
        putchar_unlocked('\n');
        // FIN DE CASO DE PRUEBA
        readInt(&pasajerosIni);
    }

    return 0;
}