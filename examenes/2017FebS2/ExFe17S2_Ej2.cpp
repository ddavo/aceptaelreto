#include <iostream>

using namespace std;

const int MAX_TAM = 100000;

// Definimos los siguientes predicados que usaremos en la poscondición
// e invariante
/*
    par(n): n (mod 2) = 0
    frec(V,n,e):    ( # i : 0 <= i < n : V[i] = e )
    perm(V,W,n): ( p.t. i : 0 <= i < n : frec(V,n,v[i]) = frec(W,n,v[i]) )
    ord(v,a,b):  ( p.t. i : a <= i < b-1 : v[i] <= v[i+1] )
*/

int inline impar(int n) {
    return n & 0b01;
}

// Si consideramos las funciones de suma, comparación, etc de coste constante
// Tenemos la función recurrente:
// T(n) = | 1               si n <= 1
//        | T(n/2) + 1      e.o.c.
// Si resolvemos la recurrencia, obtenemos una complejidad del orden O(logn)
// T(n) = T(n/2) + 1 = T(n/2^2) + 2 = T(n/2^i) + i              (1)
// Buscamos cuando se cumple el caso base:
// n/2^i = 1 sii n = 2^i sii log_2 n = i
// Sustituimos i en (1)
// T(n) = T(n/n) + log_2 n = log_2(n) + 1
// Es decir, T(n) pertenece al orden O(log n)

// { Pre: ord(v, a, b) ^ !par(b-a)
//   (ex. i : a <= i <= b ^ par(i) : v[i] != v[i+1] ) }
int solve(const int v[], int a, int b) {
    int r = (b+a)/2;

    // Caso base
    if (b - a <= 1) {
        return a;
    }

    // Casos recursivos: Pueden reducirse a 2 mediante pmd
    if (impar(r) && v[r] != v[r-1]) // Algo va mal de aquí a la izda...
        return solve(v, a, r);
    else if (impar(r) && v[r] == v[r-1]) // Va mal de aquí a la dcha...
        return solve(v, r+1, b);
    else if (v[r] != v[r+1]) // Algo va mal de aquí a la izda...
        return solve(v, a, r+1);
    else
        return solve(v, r, b);
}
// Siendo la variable libre a retornar < ret >
// (Pos: ret = min i : a <= i < b ^ par(i) ^ v[i] != v[i+1] : i )
// Es decir, el índice a partir del cual el primer elemento de una pareja no
// se situa en una posición par, o no hay pareja

int solve(const int v[], int n) {
    return solve(v, 0, n-1);
}

int main() {
    int nCasos, n;
    int v[MAX_TAM];

    cin >> nCasos;
    for (int i = 0; i < nCasos; i++) {
        cin >> n;
        for (int j = 0; j < n; j++) 
            cin >> v[j];

        cout << solve(v, n) << '\n';
    }

    return 0;
}