#include <iostream>

using namespace std;

typedef struct {
    int * arr;  // Array con los numeros
    int * nApar;  // Numero de veces que ha aparecido cada numero
} sol;

// Asumiendo que las anteriores soluciones fueron validas, comprobaremos que
// el numero que hemos puesto está separado por i veces
// Por construcción, el numero no se puede poner 2 veces

// { Pre:  }
bool esValida(sol solParcial, int k) {
    bool valida = true;
    int * v = solParcial.arr;

    for (int i = 0; i <= k && valida; i++) {
        for (int j = i+1; j <= k && valida; j++) {
            valida = !(v[i] == v[j]) || j == i+v[i]+1;
        }
    }

    return valida;
}
// { Post: valida = (p.t. i,j : 0<=i<j<=k : (v[i] = v[j]) -> j = i+v[i]+i) }

void printSol(sol sol, int n) {
    cout << sol.arr[0];
    for (int i = 1; i < n+1; i++)
        cout << " " << sol.arr[i];
    cout << "\n";
}

void solve(
        int n, // Los numeros que podemos coocar
        int k, // La iteración en la que estamos (Cota = n-k)
        int & acc,  // Numero de soluciones validas encontradas
        sol solParcial   // Solución actual
    ) {
    for (int i = 1; i < n+1; i++) {
        solParcial.arr[k] = i;

        // printf("k: %d, solP: ", k);

        if (esValida(solParcial, k)) {
            if (k < 2*n-1) {
                solve(n, k+1, acc, solParcial);
            } else {
                printSol(solParcial, k);
                acc++;
            }
        }
    }
}

int solve(int n) {
    int acc = 0;
    sol solActual {new int[n*2-2], new int[n-1]};
    solve(n, 0, acc, solActual);

    return acc;
}

int main() {
    int nCasos, n;
    cin >> nCasos;
    for (int i = 0; i < nCasos; i++) {
        cin >> n;
        int sol = solve(n);
        cout << "Soluciones: " << sol << "\n";
    }

    return 0;
}