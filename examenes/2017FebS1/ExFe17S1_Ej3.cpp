#include <iostream>
using namespace std;

void imprimirSolucion(int v[], int n, bool endl=false) {
    for (int i = 0; i < n; i++)
        printf("%d ", v[i]);

    if (endl) printf("\n");
}

void escribeCrecientesPorLosPelosDivertidos(
    const int n,    // Numero de elementos que debe tener la solución
    const int pelos,// Altura máxima de los escalones
    const int d,    // Diversión máxima
    const int e,    // Elemento a colocar en la posición k del vector si procede
    int v[],        // Solución parcial
    int k,          // Nivel de profundidad de la recursión (long sol. parcial)
    int ultCam      // Ultimo cambio que hubo en la escalera
    ) {

    int ePrima = e;
    do {   
        v[k] = ePrima;

        if (k-ultCam < d) {
            // Es d-divertido
            if (k == n-1) {
                imprimirSolucion(v, n, true);
            } else {
                escribeCrecientesPorLosPelosDivertidos(n, pelos, d, ePrima, v, k+1, ultCam);
            }
        }

        ePrima++;
        ultCam = k;
    } while (ePrima <= e+pelos && k != 0); // Para k = 0, no cambiamos el elemento
    // Volver a hacer todo
}

void escribeCrecientesPorLosPelosDivertidos(int n, int d, int e) {
    int * v = new int[n];
    escribeCrecientesPorLosPelosDivertidos(n, 1, d, e, v, 0, 0);

    delete v;
}

int main() {
    int cdp, n, d, e;
    scanf("%d", &cdp);

    for (int i = 0; i < cdp; i++) {
        scanf("%d %d %d", &n, &d, &e);

        escribeCrecientesPorLosPelosDivertidos(n, d, e);
    }

    return 0;
}