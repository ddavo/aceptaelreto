#include <iostream>
using namespace std;

const int MAX_ACTORES = 100;
const int MAX_PAPELES = 100;

typedef struct {
    int puntuacion;
    int gasto;
    int reparto[MAX_PAPELES];
} tSol;

bool operator>(const tSol & op1, const tSol & op2) {
    return op1.puntuacion > op2.puntuacion;
}

void imprimirSolucion(const tSol sol, int n) {
    printf("%d %d\n", sol.puntuacion, sol.gasto);
    for (int i = 0; i < n; i++)
        printf("%d %d\n", i, sol.reparto[i]);
}

void solve(
        int n,      // Numero de papeles de la obra
        int k,      // Numero de actores en la obra
        int p,      // Presupuesto total con el que se cuenta
        int m,       // Puntuación mínima para actual
        const int salarios[MAX_ACTORES], // Salarios de los actores
        const int puntuaciones[MAX_ACTORES][MAX_PAPELES], // Puntuaciones de los actores para cada papel
        tSol & solMax,   // Solución de puntuación máxima
        const tSol solParcial,  // Solución parcial
        int l,      // Nivel de recursión actual (longitud solución parcial)
        bool marcaje[]    // Actores que ya están en algún papel
    ) {

    for (int i = 0; i < k; i++) {
        // printf("i: %d, %s, gasto: %d, puntuacion %d\n", i, marcaje[i]?"usado":"libre", salarios[i], puntuaciones[i][l]);
        // printf("gastoAct: %d, max: %d\n", solParcial.gasto, p);
        if (    // Si es válida (es una solución para "l")
            !marcaje[i] &&      // El actor no tiene un papel asignado
            solParcial.gasto + salarios[i] <= p &&    // El salario entra en el presupuesto
            puntuaciones[i][l] >= m &&    // La puntuación no es menor que la constante
        ) {
            marcaje[i] = true;

            tSol solRecursivo = solParcial;
            solRecursivo.puntuacion += puntuaciones[i][l];
            solRecursivo.gasto += salarios[i];
            solRecursivo.reparto[l] = i;
            // imprimirSolucion(solRecursivo, l+1);

            // Si es una solución (todos los papeles han sido asignados)
            if (l == n-1) {
                // imprimirSolucion(solRecursivo, l+1);
                if (solRecursivo > solMax) {
                    solMax = solRecursivo;
                }
            } else {
                solve(n,k,p,m,salarios,puntuaciones,solMax,solRecursivo,l+1,marcaje);
            }
            
            marcaje[i] = false;
        }
    }
}

void solve(int n, int k, int p, int m, const int salarios[MAX_ACTORES], const int puntuaciones[MAX_ACTORES][MAX_PAPELES]) {
    tSol max {0, 0};
    tSol solParcial {0, 0};
    bool usados[MAX_ACTORES] = {0};
    solve(n, k, p, m, salarios, puntuaciones, max, solParcial, 0, usados);
    imprimirSolucion(max, n);
}

int main() {
    int n, k, p, m;
    int salariosActores[MAX_ACTORES];
    int puntuacionActores[MAX_ACTORES][MAX_PAPELES];

    while (scanf("%d %d %d %d", &n, &k, &p, &m) && (n || k || p || m)) {
        for (int i = 0; i < k; i++)
            scanf("%d", salariosActores+i);

        for (int i = 0; i < n; i++) 
            for (int j = 0; j < k; j++)
                scanf("%d", &puntuacionActores[j][i]);

        solve(n, k, p, m, salariosActores, puntuacionActores);

    }

    return 0;
}