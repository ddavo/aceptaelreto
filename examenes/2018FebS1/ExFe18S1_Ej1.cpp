#include <iostream>
using namespace std;

const int MAX_ELEMENTOS = 500;

// ¿A qué llamamos 'secuencia'?
// Suponemos que una secuencia viene definida por un inicio y su final

// consecutivos(v,a,b): p.t.i : a<=i<b : v[i]+1 = v[i+1]
// secuenciaMax(v,a,b): b-a+1 = max i : a<=i<j<=b ^ consecutivos(v,i,j) : j-i+1

// { Pre: 1 <= n < long(v) }
int solve(int v[], int & n /* in n, out N */ ) /* return ret */ {
    // Nota: "Si hay dos secuencias de igual longitud... -> Usaremos menor estricto"
    int i = 1;
    int ultConsecutivos = 0;
    int maxConsecutivosIni = 0;
    int maxConsecutivosSize = 1;

    // C = n-i
    // { I: 1<=i<=n ^
    //      secuenciaMax(v,maxConsecutivosIni,maxConsecutivosIni+maxConsecutivosSize) ^
    //      consecutivos(v,ultConsecutivos,i)
    // }
    while (i < n) {
        if (v[i]-1 != v[i-1]) {
            ultConsecutivos = i;
        }

        if (i - ultConsecutivos+1 > maxConsecutivosSize) {
            maxConsecutivosIni = ultConsecutivos;
            maxConsecutivosSize = i - ultConsecutivos+1;
        }

        ++i;
    }

    n = maxConsecutivosSize;
    return maxConsecutivosIni;
}
// { Pos: secuenciaMax(v,ret,ret+N) }

int main() {
    int n, sol;
    int v[MAX_ELEMENTOS];

    scanf("%d", &n);
    while (n) {
        for (int i = 0; i < n; ++i)
            scanf("%d", v+i);

        sol = solve(v, n);
        printf("%d %d\n", sol, n);

        scanf("%d", &n);
    }

    return 0;
}