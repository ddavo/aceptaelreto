#include <iostream>
using namespace std;

#ifndef MAX_SIZE
#define MAX_SIZE 500
#endif

// Para calcular la complejidad del algoritmo, vamos a resolver la recurrencia:
// Siendo n el tamaño del problema tal que n = b-a
// T(n) la función del tiempo en cuestión del tamaño del problema en el peor de
// los casos (el caso mejor es cuando el elemento buscado está en el medio y tiene O(1))
// T(n) = | 1               si n <= 1
//        | T(n/2) + 1      e.o.c.
// T(n) = T(n/2) + 1 = T(n/4) + 2 = T(n/8) + 3 = T(n/2^i) + i (a)
// T(n/2^i) = 1 sii n/2^i <= 1 sii n <= 2^i sii log_2 n = i
// Sustituimos en la formula de la recurrencia (a)
// T(n) = T(n/(2^{log_2 n})) + log_2(n) = log_2(n) + 1
// Por lo tanto, la función T(n) pertenece al orden O(log(n))
// Ya que \lim_n\rightarrow\inf \dfrac{\log_2(n)}{\log n}=K\in \mathbb{R}

int solve(int v[], int a, int b, int x) {

    // printf("a: %d, b: %d, v[a]: %d, v[b]: %d, x: %d\n", a, b, v[a], v[b], x);
    if (b-a <= 1) {
        return (abs(v[a]-x)<=abs(v[b]-x))?v[a]:v[b];
    }

    int m = (a+b)/2;
    if (v[m] == x) return x;

    if (abs(v[a]-x)<=abs(v[b]-x)) {
        return solve(v,a,m,x);
    } else {
        return solve(v,m,b,x);
    }
}

int solve(int v[], int n, int x) {
    return solve(v, 0, n-1, x);
}

int main() {
    int cdp, x, n;
    int v[MAX_SIZE];

    scanf("%d", &cdp);

    for (int i = 0; i < cdp; i++) {
        scanf("%d", &x);
        scanf("%d", &n);
        for (int j = 0; j < n; j++)
            scanf("%d", &v[j]);

        printf("%d\n", solve(v, n, x));
    }
    
    return 0;
}