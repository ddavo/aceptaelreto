#include <iostream>
using namespace std;

#ifndef MAX_SIZE
#define MAX_SIZE 500
#endif

// Predicados que usaremos en el ejercicio
// decr(v,a,b): p.t.i : a<=i<b : v[i]>=v[i+1]

// { Pre: 1<=n<long(v) }
int solve(int v[], int n) { /* return ret */
    int i = 1;
    int currentIni = 0;
    int maxW = 1;

    // Cota: C=n-i
    // El algoritmo es de coste lineal O(n)

    // 1<=i<=n ^
    // 0<= currentIni <= i ^
    // decr(v,currentIni,i) ^
    // maxW = (max k,k: 0<=k<l<i ^ decr(v,k,l) : k-l+1)
    while (i < n) {
        if (v[i-1] < v[i])  // No es decreciente
            currentIni = i;

        if (i - currentIni+1 > maxW)
            maxW = i - currentIni+1;

        ++i;
    }

    return maxW;
}
// { ret = (max i,j : 0<=i<j<n ^ decr(v,i,j) : j-i+1) }

int main() {
    int cdp, n;
    int v[MAX_SIZE];

    scanf("%d", &cdp);
    for (int i = 0; i < cdp; i++) {
        scanf("%d", &n);
        for (int j = 0; j < n; j++)
            scanf("%d", &v[j]);

        printf("%d\n", solve(v, n));
    }

    return 0;
}
