#include <iostream>
using namespace std;

const int MAX_MAQUINAS = 50;
const int MAX_CARRETERAS = 50;

// Función auxiliar de "debug"
void printv(int v[], int n) {
    printf("%d", v[0]);
    for (int i = 1; i <= n; i++)
        printf(" %d", v[i]);
    printf("\n");
}

void solve(
    int maquinas[MAX_MAQUINAS],     // La anchura de las maquinas
    int m,                          // Numero de maquinas totales
    int carreteras[MAX_CARRETERAS], // Anchura de las carreteras
    int n,                          // Numero de carreteras
    int calidades[MAX_MAQUINAS][MAX_CARRETERAS],    // Afinidad de una maquina a una carretera
    int k,                          // Profundidad de la recursión. Longitud de la solución parcial
    int & maxCalidad,               // Calidad máxima encontrada hasta el momento
    int actCalidad,                 // Calidad de la solución parcial actual
    int maquinasUsadas[MAX_MAQUINAS],               // Maquinas usadas y carretera asignada
    bool carreterasUsadas[MAX_CARRETERAS]           // Marcaje: Si la carretera ha sido usada o no
    ) {

    for (int i = 0; i < n; ++i) {
        if (!carreterasUsadas[i] && // Si la carretera no está asignada
            maquinas[k] <= carreteras[i]) { // Si la máquina "cabe" por la carretera

            maquinasUsadas[k] = i;
            carreterasUsadas[i] = true;

            if (k == m-1) {
                // Todas las máquinas se han usado, imprimimos la solución en pantalla
                // printf("q: %d v: ", actCalidad+calidades[k][i]);
                // printv(maquinasUsadas, k);
                if (actCalidad + calidades[k][i] > maxCalidad) maxCalidad = actCalidad + calidades[k][i];
            } else {
                solve(maquinas, m, carreteras, n, calidades, k+1, maxCalidad,
                    actCalidad + calidades[k][i], maquinasUsadas, carreterasUsadas);
            }

            carreterasUsadas[i] = false;
        }
    }
}

void solve(int maquinas[MAX_MAQUINAS], int m, int carreteras[MAX_CARRETERAS], int n,
    int calidades[MAX_MAQUINAS][MAX_CARRETERAS]) {

    int maxCalidad = 0;
    int maquinasUsadas[MAX_MAQUINAS];
    bool carreterasUsadas[MAX_CARRETERAS] {0};
    solve(maquinas, m, carreteras, n, calidades, 0, maxCalidad, 0, maquinasUsadas, carreterasUsadas);
    printf("%d\n", maxCalidad);
}

int main() {
    int cdp, n, m;
    int maquinas[MAX_MAQUINAS];
    int carreteras[MAX_CARRETERAS];
    int calidades[MAX_MAQUINAS][MAX_CARRETERAS];

    scanf("%d", &cdp);
    for (int i = 0; i < cdp; ++i) {
        scanf("%d %d", &m, &n);
        for (int j = 0; j < m; ++j)
            scanf("%d", &maquinas[j]);
        for (int j = 0; j < n; ++j)
            scanf("%d", &carreteras[j]);

        for (int linea = 0; linea < m; linea++)
            for (int col = 0; col < n; col++)
                scanf("%d", &calidades[linea][col]);

        solve(maquinas, m, carreteras, n, calidades);
    }

    return 0;
}