#include <iostream>
using namespace std;

const int MAX_SIZE = 50000;

// ordex(v, n) : p.t i : 0<=i<n-1 : v[i] < v[i+1]

// De la cota deducimos que el algoritmo es de coste O(n) (lineal)
// Tanto en el mejor, como en el peor de los casos y en el caso promedio

// { Pre: ordex(v, n) ^ 1 <= n < tam(v) }
void solve(int v[] /* in v, out V */, int & n /* in n, out N */) {
    int i = 0;
    int nPares = 0;

    // Cota: C = n-i
    // { I: nPares = ( # k : 0<=k<i : v[i]mod2=0 ) ^
    //      0 <= i <= n ^
    //      ( p.t k : 0<=k<i : v[k]mod2=0 ) }
    while (i < n) {
        if (v[i] % 2 == 0) { // Si es par
            v[nPares] = v[i];
            nPares++;
        }

        ++i;
    }

    n = nPares;
}
// { Pos: (N = # i : 0 <= i < n : v[i]mod2=0) ^ 
// (p.t. i : 0 <= i < n : (v[i]mod2=0)->(ex. j: 0<=j<N : V[j]=v[i])) ^
// ordex(v,N) }

int main() {
    int n;
    int v[MAX_SIZE];
    cin >> n;

    while (n != -1) {
        for (int i = 0; i < n; i++)
            cin >> v[i];

        solve(v, n);
        for (int i = 0; i < n; i++)
            cout << v[i] << " ";
        cout << "\n";

        cin >> n;
    }

    return 0;
}