#include <iostream>
using namespace std;

#define MAX_CASOS 5000

int solve(int v[], int a, int b, int m) {
    printf("a: %d, b: %d, m: %d\n", a, b, m);

    if (v[a] == v[b]) // Caso base
        return 1;
}

int solve(int v[], int n) {
    return solve(v, 0, n-1, (n-1)/2);
}

int main() {
    int n;
    int v[MAX_CASOS];

    cin >> n;
    while (n) {
        for (int i = 0; i < n; i++)
            cin >> v[i];

        cout << solve(v, n) << "\n";

        cin >> n;
    }

    return 0;
}