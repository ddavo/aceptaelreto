#include <iostream>
using namespace std;

typedef enum {ROJO, VERDE, AZUL} tColor;

void imprimirSolucion(tColor torre[], int k) {
    for (int i = 0; i < k; i++) {
        switch(torre[i]) {
            case ROJO: cout << "rojo "; break;
            case VERDE: cout << "verde "; break;
            case AZUL: cout << "azul "; break;
        }
    }
}

// Traducción literal sin optimizar del ejercicio
bool esValida(tColor torre[], int k) {
    int cnt[3] = {0};
    bool valida = torre[0] == ROJO;

    for (int i = 0; i < k && valida; i++) {
        // v[i] = verde -> v[i+1] != verde
        valida = torre[i] != VERDE || i+1 == k || torre[i+1] != VERDE;
        cnt[torre[i]]++;
    }

    return valida && cnt[VERDE] <= cnt[AZUL];
}

bool esSolucion(tColor torre[], int k) {
    int cnt[3] = {0};
    for (int i = 0; i < k; i++) {
        cnt[torre[i]]++;
    }

    return esValida(torre, k) && cnt[ROJO] > cnt[AZUL] + cnt[VERDE];
}

bool solve(
        int n,  // Altura final de la torre
        int restantes[3],    // Cubos restantes para cada color
        tColor torre[],   // Torre actual (0...k)
        int k   // Profundidad de la recursión
    ) {

    if (k == n && esSolucion(torre, k)) {
        imprimirSolucion(torre, k);
        cout << "\n";
        return true;
    }

    bool haySolucion = false;
    for (int i = ROJO; i < 3; i++) {
        if (restantes[i] > 0) {
            restantes[i]--;
            torre[k] = (tColor)i;

            if (esValida(torre, k)) {
                haySolucion = solve(n, restantes, torre, k+1) || haySolucion;
            }
            restantes[i]++;
        }
    }

    return haySolucion;
}

bool solve(int n, int r, int g, int b) {
    int k = 0;
    tColor * torre = new tColor[n];
    int restantes[3];
    restantes[ROJO] = r;
    restantes[VERDE] = g;
    restantes[AZUL] = b;
    bool ret = solve(n, restantes, torre, k);
    delete torre;
    return ret;
}

int main() {
    int n, r, g, b;
    cin >> n >> b >> r >> g;
    while (n || r || g || b) {
        if (!solve(n, r, g, b))
            cout << "SIN SOLUCION\n";

        cin >> n >> b >> r >> g;
    }

    return 0;
}